package rotatearray;

//STUDENT: ELVIS JOSE CASTRO
public class Rotator {

    private static final int MINIMUM_POSITION = 0;

    public Object[] rotate(Object[] numbers, int numbersOfPositions){

        Object[] arrayAux = new Object[numbers.length];
        int currentPosition = firstPosition(numbers, numbersOfPositions);

        for (Object number : numbers) {
            arrayAux[currentPosition] = number;
            currentPosition += 1;
            currentPosition = nextPositionRight(numbers, currentPosition);
        }
        return arrayAux;
    }

    private boolean isPositive(int numberToVerify){
        return numberToVerify > 0;
    }

    private boolean isNegative(int numberToVerify){
        return numberToVerify < 0;
    }

    private int firstPosition(Object[] numbers, int numberOfPositions){
        int position = 0;
        numberOfPositions = simplifyNumberOfPositions(numbers,numberOfPositions);
        int positiveNumber = convertToPositive(numberOfPositions);
        while (positiveNumber > 0) {
            if (isPositive(numberOfPositions)) {
                position = swipeRight(numbers, position);
            } else if (isNegative(numberOfPositions)) {
                position = swipeLeft(numbers, position);
            }
            positiveNumber --;
        }
        return position;
    }

    private int swipeLeft (Object[] numbers, int position){
        int positionMax = numbers.length - 1;
        if (position == MINIMUM_POSITION){
            return positionMax;
        }
        return position - 1;
    }

    private int swipeRight (Object[] numbers, int position){
        int positionMax = numbers.length - 1;
        if(position == positionMax){
            return MINIMUM_POSITION;
        }
        return position + 1;
    }

    private int nextPositionRight(Object[] numbers, int currentPosition){
        int positionMax = numbers.length - 1;
        int distance = currentPosition - positionMax;
        if(isGreaterThan(currentPosition, positionMax) && distance == 1){
            currentPosition = 0;
        }
        return currentPosition;
    }

    private boolean isGreaterThan(int number1, int number2){
        return number1 > number2;
    }

    private int convertToPositive (int number){
        if (!isPositive(number)){
            number *= -1;
        }
        return number;
    }

    private int simplifyNumberOfPositions(Object[] numbers, int numbersOfPositionsToSimplify){
        int length = numbers.length ;
        if(numbersOfPositionsToSimplify > length){
            numbersOfPositionsToSimplify %= length;
        }
        return numbersOfPositionsToSimplify;
    }
}
